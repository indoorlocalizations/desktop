package cz.sodae.leguide.geoprojection

import cz.sodae.leguide.geoprojection.graph.OverviewGraph
import cz.sodae.leguide.geoprojection.graph2.GraphBuilder
import cz.sodae.leguide.geoprojection.graph2.PathFinder
import cz.sodae.leguide.geoprojection.loader.Loader
import cz.sodae.leguide.geoprojection.process.CurrentPosition
import cz.sodae.leguide.geoprojection.process.ReactiveProcess
import cz.sodae.leguide.geoprojection.process.Signal
import cz.sodae.leguide.geoprojection.utils.convertToCircleAngle
import io.reactivex.Flowable
import org.locationtech.jts.awt.PointTransformation
import org.locationtech.jts.awt.ShapeWriter
import org.locationtech.jts.geom.Coordinate
import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.io.File
import java.util.concurrent.TimeUnit
import javax.swing.JFrame
import javax.swing.JPanel

class DrawShapes(val fileLog: String, val fileMap: String, val zoomOffset: Int, val zoomScale: Int) : JFrame() {

    /**
     * Loads maps
     */
    private val loader: Loader by lazy {
        val json = File(fileMap).readText()
        Loader(json)
    }

    /**
     * Generates paths
     */
    private val pathFinder = loader.let {
        val graphBuilder = GraphBuilder(loader.building, OverviewGraph(loader.building))
        PathFinder(loader.building, graphBuilder.exportToGraphStructure())
    }

    private val center: Coordinate = loader.center

    private lateinit var reactiveProcess: ReactiveProcess

    private var currentPosition: CurrentPosition? = null

    /**
     * Split log file by lines
     * Produces values with time span
     * Shows result
     */
    fun providing() {
        val samples = File(fileLog)
                .readText()
                .split("\n")
                .filter { !it.contains("timestampMs") && !it.isEmpty() }
                .map { it.split(";") }

        val a = Flowable
                .fromIterable(samples)
                .buffer(2, 1)
                .filter { it.size == 2 }
                .concatMap({ i ->
                    Flowable.just(i[1]).delay(i[1][0].toLong() - i[0][0].toLong(), TimeUnit.MILLISECONDS)
                })
                .share()

        reactiveProcess = ReactiveProcess(
                a.map { it[1].toInt().rem(2) == 1 },
                a.filter { it[1].toInt() in 70..71 }.map {
                    convertToCircleAngle(Math.toDegrees(it[3].toDouble()))
                },
                a.map { Signal(it[2], it[3].toDouble()) },
                loader.building,
                pathFinder
        )
        reactiveProcess.debugging = true
        reactiveProcess.positionReactive.subscribe {
            currentPosition = it
            panel.repaint()
        }
    }

    fun scale(p: Double): Double {
        return p * zoomScale
    }

    fun zoom(p: Double): Double {
        return zoomOffset + scale(p)
    }

    val panel = object : JPanel() {
        public override fun paintComponent(g: Graphics) {
            val g2 = g as Graphics2D

            g.color = this.background
            g.fillRect(0, 0, this.width, this.height);

            g.color = Color.BLACK

            currentPosition?.let {
                g2.draw(ShapeWriter(PointTransformation { src, dest ->
                    dest!!.setLocation(
                            zoom(src!!.x - center.x),
                            zoom(src!!.y - center.y)
                    )
                }).toShape(loader.building.getGeometryOnLevel(it.level)))

                it.debugging?.let {
                    g2.color = Color.MAGENTA.brighter()
                    it.travelEdges?.forEach {
                        g2.drawPolyline(
                                listOf(it.first, it.second).map { zoom(it.x - center.x).toInt() }.toIntArray(),
                                listOf(it.first, it.second).map { zoom(it.y - center.y).toInt() }.toIntArray(),
                                2
                        )
                    }
                }

                class _P(val x: Int, val y: Int)

                it.debugging?.let {

                    g2.color = Color.ORANGE

                    val zoomedPositionOld = _P(
                            zoom(it.beaconsPFPoint.x - center.x).toInt(),
                            zoom(it.beaconsPFPoint.y - center.y).toInt()
                    )

                    g2.fillOval(
                            zoomedPositionOld.x, zoomedPositionOld.y, 10, 10
                    )

                    val distanceStandardDeviationPxOld = scale(it.beaconsPFAccuracy).toInt()
                    g2.drawOval(
                            zoomedPositionOld.x - distanceStandardDeviationPxOld,
                            zoomedPositionOld.y - distanceStandardDeviationPxOld,
                            distanceStandardDeviationPxOld * 2, distanceStandardDeviationPxOld * 2
                    )

                    it.particles.forEach {
                        g2.drawOval(zoom(it.x - center.x).toInt(), zoom(it.y - center.y).toInt(), 3, 3)
                    }

                    it.beaconsRange.forEach {
                        g2.color = Color.RED
                        val x = zoom(it.beaconPosition.x - center.x).toInt()
                        val y = zoom(it.beaconPosition.y - center.y).toInt()
                        val size = scale(it.distance).toInt()
                        g2.drawOval(x - size, y - size, size * 2, size * 2)
                        g2.drawString("%.3f".format(it.distance) + "m", x - 10, y)

                        g2.drawOval(x, y, 5, 5)
                    }

                    it.travelPath?.let {
                        g2.color = Color.BLUE

                        it.forEach {
                            g2.drawOval(zoom(it.x - center.x).toInt(), zoom(it.y - center.y).toInt(), 1, 1)
                        }

                        g2.drawPolyline(
                                it.map { zoom(it.x - center.x).toInt() }.toIntArray(),
                                it.map { zoom(it.y - center.y).toInt() }.toIntArray(),
                                it.size
                        )
                    }

                }


                g2.color = Color.GREEN

                if (it.movement) {
                    g2.color = Color.GREEN.darker().darker()
                }

                val zoomedPosition = _P(
                        zoom(it.point.x - center.x).toInt(),
                        zoom(it.point.y - center.y).toInt()
                )

                g2.fillOval(
                        zoomedPosition.x, zoomedPosition.y, 10, 10
                )

                g2.drawLine(
                        zoomedPosition.x,
                        zoomedPosition.y,
                        zoomedPosition.x + (30 * Math.sin(
                                Math.toRadians(convertToCircleAngle(it.compass))
                        )).toInt(),
                        zoomedPosition.y + (30 * Math.cos(
                                Math.toRadians(convertToCircleAngle(it.compass))
                        )).toInt()
                )

                val distanceStandardDeviationPx = scale(it.accuracy).toInt()
                g2.drawOval(
                        zoomedPosition.x - distanceStandardDeviationPx,
                        zoomedPosition.y - distanceStandardDeviationPx,
                        distanceStandardDeviationPx * 2, distanceStandardDeviationPx * 2
                )


            }
        }
    }

    init {

        size = Dimension(1000, 1000)
        defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        isVisible = true
        title = "Debugger"
        this.contentPane.add(panel)
    }

    companion object {

        private val serialVersionUID = 1L

        @JvmStatic
        fun main(arg: Array<String>) {
            println("Usage: LOG_FILE MAP_FILE [ZOOM_OFFSET] [ZOOM_SCALE]")
            println(" - LOG_FILE is csv format")
            println(" - MAP_FILE is geojson format")
            println(" - ZOOM_OFFSET: default value is 500")
            println(" - ZOOM_SCALE: default value is 20")
            println("Tools accepts format from BLE Measuring app (requires one value '70' in relevant column at least)")
            println("Example files are placed in example directory")
            println("Note: Sometimes, program runs wrong and it shows nothing. Restart it.")

            if (arg.size >= 2) {
                DrawShapes(
                        arg[0],
                        arg[1],
                        arg.getOrNull(2)?.toIntOrNull() ?: 500,
                        arg.getOrNull(3)?.toIntOrNull() ?: 20
                ).providing()
            }
        }
    }

}